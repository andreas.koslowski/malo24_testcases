# MaLo24_testcases

**(Ich selber bin nicht vom Lehrstuhl und _höre_ Malo nur)**

Das Teilen von Testcases ist erlaubt, solange kein Teil der Lösung geteilt wird [source](https://moodle.rwth-aachen.de/mod/hsuforum/discuss.php?d=6602)

Bei Fragen, Anmerkungen, Hinweisen auf Fehler oder falls ihr weitere Testcases habt: luis.matzke@rwth-aachen.de

Ihr könnt das repo auch forken Tests hinzufügen und anschließend mergerequests stellen.

## Abgabe 5

### evaluate_term

```py
f, c, x = FunctionSymbol("F", 1), FunctionSymbol("C", 0), Variable("x")
g = FunctionSymbol("G", 2)
some_interpretation = Interpretation(
    Structure(range(10), {f: lambda n: n + 1, c: 5, g: lambda x, y: x - y}),
    {x: 3},
)

@custom_test((x, some_interpretation), 3)
@custom_test((f(x), some_interpretation), 4)
@custom_test((c(), some_interpretation), 5)
@custom_test((g(x,x), some_interpretation), 0)
@custom_test((f(g(c(), f(x))), some_interpretation), 2)
```

### evaluate_atom

```py
f, c, x, = FunctionSymbol("F", 1), FunctionSymbol("C", 0), Variable("x")
g = FunctionSymbol("G", 2)
y, z = Variable("y"), Variable("z")
r = RelationSymbol("R", 2)
t, nt = TruthConstant(True), TruthConstant(False)
some_interpretation = Interpretation(
    Structure(range(10), {f: lambda x: x + 1, c: 5, g: lambda x, y: x - y, r: lambda x, y: x < y}),
    {x: 3, y: 5, z: 7},
)

@custom_test((f(x) == c, some_interpretation), False)
@custom_test((f(f(x)) == c, some_interpretation), True)
@custom_test((f(x) == f(x), some_interpretation), True)
@custom_test((f(x) == f(g(c(), f(x))), some_interpretation), False)
@custom_test((f(x) == f(g(c(), f(g(c(), f(x))))), some_interpretation), True)
@custom_test((r(x, y), some_interpretation), True)
@custom_test((r(x, c()), some_interpretation), True)
@custom_test((r(z, y), some_interpretation), False)
@custom_test((r(f(f(f(x))), y), some_interpretation), False)
@custom_test((r(z, f(f(f(x)))), some_interpretation), False)
@custom_test((r(z, f(f(f(f(f(x)))))), some_interpretation), True)
@custom_test((t, some_interpretation), True)
@custom_test((nt, some_interpretation), False)
```

## Abgabe 4

### choose_literal

```py
X = Symbol("X")
Y = Symbol("Y")
@custom_test(BigAnd(BigOr(X)), X, "positive literal") # (X) initial
@custom_test(BigAnd(BigOr(Neg(X))), Neg(X), "negative literal") 
@custom_test(BigAnd(BigOr(Neg(X), Y)), [Y, Neg(X)])
@custom_test(BigAnd(BigOr(Y), BigOr(Neg(X))), [Y, Neg(X)])
```

### dpll

```py
X = Symbol("X")
Y = Symbol("Y")

# initial test
# (X /\ ~X)
@custom_test(BigAnd(BigOr(X), BigOr(~X)), None)

#
# ((X \/ ~X) /\ (X \/ ~X))
@custom_test(
    BigAnd(BigOr(Symbol("X"),Neg(Symbol("X"))),BigOr(Symbol("X"),Neg(Symbol("X")))),
    [{Symbol(label='X'): True},{Symbol(label='X'): False}]
)

# Testing multiple empty klauseln
# (X /\ ~X /\ ~X /\ X)
@custom_test( BigAnd( BigOr(Symbol("X")), BigOr(Neg(Symbol("X"))), BigOr(Neg(Symbol("X"))),BigOr(Symbol("X")) ), None )

# Formula from Lecture
# Test verifies by checking if truth table evaluates the formula to true
# Therefore less strict but more edge cases.
# ((P1 \/ ~P5 \/ ~P6 \/ P7) /\ (~P1 \/ P2 \/ ~P5) /\ (~P1 \/ ~P2 \/ ~P3 \/ ~P5 \/ ~P6) /\ (P1 \/ P2 \/ ~P4 \/ P7) /\ (~P4 \/ ~P6 \/ ~P7) /\ (P3 \/ ~P5 \/ P7) /\ (P3 \/ ~P4 \/ ~P5) /\ (P5 \/ ~P6) /\ (P5 \/ P4 \/ ~P8) /\ (P1 \/ P3 \/ P5 \/ P6 \/ P7) /\ (~P7 \/ P8) /\ (~P6 \/ ~P7 \/ ~P8))
@custom_test(inp := BigAnd([
    BigOr(Symbol("P1"),Neg(Symbol("P5")),Neg(Symbol("P6")),Symbol("P7")),
    BigOr(Neg(Symbol("P1")),Symbol("P2"),Neg(Symbol("P5"))),
    BigOr(Neg(Symbol("P1")),Neg(Symbol("P2")),Neg(Symbol("P3")),Neg(Symbol("P5")),Neg(Symbol("P6"))),
    BigOr(Symbol("P1"),Symbol("P2"),Neg(Symbol("P4")),Symbol("P7")),
    BigOr(Neg(Symbol("P4")),Neg(Symbol("P6")),Neg(Symbol("P7"))),
    BigOr(Symbol("P3"),Neg(Symbol("P5")),Symbol("P7")),
    BigOr(Symbol("P3"),Neg(Symbol("P4")),Neg(Symbol("P5"))),
    BigOr(Symbol("P5"),Neg(Symbol("P6"))),
    BigOr(Symbol("P5"),Symbol("P4"),Neg(Symbol("P8"))),
    BigOr(Symbol("P1"),Symbol("P3"),Symbol("P5"),Symbol("P6"),Symbol("P7")),
    BigOr(Neg(Symbol("P7")),Symbol("P8")),
    BigOr(Neg(Symbol("P6")),Neg(Symbol("P7")),Neg(Symbol("P8")))
    ]),
    lambda out: inp.evaluate( out ),
    "DPLL example formula from lecture"
)

# ((~Q \/ ~P \/ ~T \/ R) /\ P /\ S /\ (~R \/ ~Q \/ ~P) /\ T /\ (Q \/ ~P \/ ~T))
@custom_test(BigAnd([
    BigOr(Neg(Symbol("Q")),Neg(Symbol("P")),Neg(Symbol("T")),Symbol("R")),
    BigOr(Symbol("P")),
    BigOr(Symbol("S")),
    BigOr(Neg(Symbol("R")),Neg(Symbol("Q")),Neg(Symbol("P"))),
    BigOr(Symbol("T")),
    BigOr(Symbol("Q"),Neg(Symbol("P")),Neg(Symbol("T")))
]), None )

# ((R \/ ~P \/ ~S) /\ R /\ (S \/ ~R \/ ~Q) /\ P /\ (~R \/ P \/ ~T) /\ (~R \/ S \/ ~P))
@custom_test(inp2 := BigAnd([
    BigOr(Symbol("R"),Neg(Symbol("P")),Neg(Symbol("S"))),
    BigOr(Symbol("R")),
    BigOr(Symbol("S"),Neg(Symbol("R")),Neg(Symbol("Q"))),
    BigOr(Symbol("P")),
    BigOr(Neg(Symbol("R")),Symbol("P"),Neg(Symbol("T"))),
    BigOr(Neg(Symbol("R")),Symbol("S"),Neg(Symbol("P")))
    ]),
    lambda out: inp2.evaluate( out )
)

@custom_test(BigAnd(
    BigOr(X,Y),
    BigOr(~X,~Y),
    BigOr(X,~Y),
    BigOr(~X,Y)
), None, "Algo l. 12")
```